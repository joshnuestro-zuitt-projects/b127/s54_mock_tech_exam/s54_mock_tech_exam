let collection = [];
let tempCol = []

// Write the queue functions below.

function print() {
   return collection;
}

function enqueue(element) {
    // collection.push(element);
    collection[collection.length] = element
    return collection;
}

function dequeue() {
    // collection.shift();
    for(i=0;i<(collection.length-1);i++){
      tempCol[i] = collection[i+1]
    }
    collection = tempCol
    return collection;
}

function front() {
    return collection[0];
}

function size() {
   return collection.length;
}

function isEmpty() {
    return collection.length === 0;
}

 
module.exports = {
   collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};